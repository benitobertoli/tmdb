package com.benitobertoli.tmdb.util

import java.text.NumberFormat
import java.util.*


fun Int.format(): String {
    return NumberFormat.getInstance(Locale.US).format(this)
}