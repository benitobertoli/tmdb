package com.benitobertoli.tmdb.util

import java.util.*


fun Calendar.sameDay(calendar: Calendar): Boolean {
    return get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
            && get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
            && get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
}