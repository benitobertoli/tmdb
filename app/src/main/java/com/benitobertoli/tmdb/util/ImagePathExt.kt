package com.benitobertoli.tmdb.util

import com.benitobertoli.tmdb.data.api.ApiContract


fun String.imageUrl(size: String): String {
    return ApiContract.BASE_URL_IMAGE + size + this
}

fun String.backdropSmall(): String {
    return this.imageUrl(ApiContract.BACKDROP.W_300)
}

fun String.backdropLarge(): String {
    return this.imageUrl(ApiContract.BACKDROP.W_780)
}

fun String.posterSmall(): String {
    return this.imageUrl(ApiContract.POSTER.W_342)
}

fun String.posterLarge(): String {
    return this.imageUrl(ApiContract.POSTER.W_500)
}

fun String.logoSmall(): String {
    return this.imageUrl(ApiContract.LOGO.W_154)
}

fun String.logoLarge(): String {
    return this.imageUrl(ApiContract.LOGO.W_300)
}