package com.benitobertoli.tmdb

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    InjectorsModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent {
    fun inject(app: App)
}
