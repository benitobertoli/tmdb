package com.benitobertoli.tmdb

import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import com.benitobertoli.tmdb.ui.blocks.LoadingLayout
import com.benitobertoli.tmdb.ui.blocks.LoadingView
import com.benitobertoli.tmdb.ui.blocks.ToolbarLayout
import com.benitobertoli.tmdb.util.DialogUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {
    private var toolbarLayout: ToolbarLayout? = null
    protected var toolbar: Toolbar? = null
        get() = toolbarLayout?.toolbar
        private set
    private var loadingLayout: LoadingLayout? = null
    protected var loadingView: LoadingView? = null
        get() = loadingLayout?.loadingView
        private set

    protected var progressDialog: ProgressDialog? = null

    private var rxDisposables: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    /**
     * Dagger 2 relies on strongly typed classes so we must explicitly inject
     * each subclass.
     */
    protected abstract fun inject()

    protected val activity: BaseActivity
        get() = this

    fun setContentView(view: View, wrapLoading: Boolean, wrapToolbar: Boolean, toolbarLayoutId: Int) {
        setContentView(wrap(view, wrapLoading, wrapToolbar, toolbarLayoutId, R.id.toolbar))
    }

    fun setContentView(layoutResId: Int, wrapLoading: Boolean, wrapToolbar: Boolean, toolbarLayoutId: Int) {
        val view = layoutInflater.inflate(layoutResId, null)
        setContentView(wrap(view, wrapLoading, wrapToolbar, toolbarLayoutId, R.id.toolbar))
    }

    override fun setContentView(layoutResID: Int) {
        setContentView(layoutResID, true, true, R.layout.blocks_toolbar_default)
    }

    fun wrap(view: View, wrapLoading: Boolean, wrapToolbar: Boolean, toolbarLayoutId: Int, toolbarId: Int): View {
        var v = view
        if (wrapLoading) {
            loadingLayout = LoadingLayout(this)
            loadingLayout?.let {
                it.addView(v, 0)
                v = it
            }
        }

        if (wrapToolbar) {
            toolbarLayout = ToolbarLayout(this, toolbarLayoutId, toolbarId)
            toolbarLayout?.let {
                it.addView(v, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                setSupportActionBar(toolbar)
                v = it
            }
        }

        return v
    }

    protected fun addDisposable(disposable: Disposable) {
        if (rxDisposables == null) {
            rxDisposables = CompositeDisposable()
        }
        rxDisposables?.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        rxDisposables?.dispose()
    }

    fun showContent() {
        loadingLayout?.showContent()
    }

    fun showInlineLoading() {
        loadingLayout?.showLoadingView()
        loadingView?.loading(true)
    }

    fun showInlineMessage(message: String) {
        loadingLayout?.showLoadingView()
        loadingView?.loading(false)
                ?.message(message)
                ?.displayButton(false)
    }

    fun showInlineMessage(messageRes: Int) {
        showInlineMessage(getString(messageRes))
    }

    fun showInlineMessageWithAction(message: String, actionName: String, action: (() -> Unit)?) {
        loadingLayout?.showLoadingView()
        loadingView?.loading(false)
                ?.message(message)
                ?.displayButton(true)
                ?.buttonText(actionName)
                ?.buttonClickListener(View.OnClickListener { action?.invoke() })
    }

    fun showInlineMessageWithAction(message: String, actionNameRes: Int, action: (() -> Unit)?) {
        showInlineMessageWithAction(message, getString(actionNameRes), action)
    }

    fun showInlineMessageWithAction(messageRes: Int, actionName: String, action: (() -> Unit)?) {
        showInlineMessageWithAction(getString(messageRes), actionName, action)
    }

    fun showInlineMessageWithAction(messageRes: Int, actionNameRes: Int, action: (() -> Unit)?) {
        showInlineMessageWithAction(getString(messageRes), getString(actionNameRes), action)
    }

    fun showProgressDialog(message: String) {
        progressDialog = DialogUtils.showLoadingDialog(this, message)
    }

    fun showProgressDialog(messageRes: Int) {
        progressDialog = DialogUtils.showLoadingDialog(this, getString(messageRes))
    }

    fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

    fun showMessage(message: String) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                .show()
    }

    fun showMessage(messageRes: Int) {
        showMessage(getString(messageRes))
    }

    fun showMessageWithAction(message: String, actionName: String, action: (() -> Unit)?) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE)
                .setAction(actionName) { action?.invoke() }
                .show()
    }

    fun showMessageWithAction(message: String, actionNameRes: Int, action: (() -> Unit)?) {
        showMessageWithAction(message, getString(actionNameRes), action)
    }
}
