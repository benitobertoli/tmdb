package com.benitobertoli.tmdb

import com.benitobertoli.tmdb.exception.AppException
import com.benitobertoli.tmdb.exception.AppExceptionFactory
import io.reactivex.observers.DisposableObserver

/**
 * Default [DisposableObserver] base class to be used whenever you want default error handling.
 */
abstract class DefaultObserver<T>(private val appExceptionFactory: AppExceptionFactory) : DisposableObserver<T>() {
    override fun onComplete() {
        // no-op by default.
    }

    override fun onError(t: Throwable) {
        onError(appExceptionFactory.make(t))
    }

    abstract fun onError(e: AppException)
}
