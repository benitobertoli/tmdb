package com.benitobertoli.tmdb.features.moviedetail

import android.arch.lifecycle.MutableLiveData
import com.benitobertoli.tmdb.DefaultObserver
import com.benitobertoli.tmdb.data.api.dto.MovieDetailsDto
import com.benitobertoli.tmdb.exception.AppException
import com.benitobertoli.tmdb.exception.AppExceptionFactory
import com.benitobertoli.tmdb.features.moviedetail.domain.GetMovieDetailsUseCase
import com.benitobertoli.tmdb.ui.BaseViewModel
import javax.inject.Inject

class MovieDetailsViewModel
@Inject constructor(
        private val getMovieDetails: GetMovieDetailsUseCase,
        private val appExceptionFactory: AppExceptionFactory
) : BaseViewModel() {

    val movieData = MutableLiveData<MovieDetailsDto>()

    fun getMovieDetails(movieId: Long) {
        if (movieData.value != null && movieData.value?.id == movieId) return
        showInlineLoading()
        getMovieDetails.execute(movieId, buildMovieObserver(movieId))
    }

    private fun buildMovieObserver(movieId: Long): DefaultObserver<MovieDetailsDto> {
        return object : DefaultObserver<MovieDetailsDto>(appExceptionFactory) {

            override fun onNext(movie: MovieDetailsDto) {
                showContent()
                movieData.value = movie
            }

            override fun onError(e: AppException) {
                showInlineMessage(e.userMessage) { getMovieDetails(movieId) }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        getMovieDetails.dispose()
    }
}
