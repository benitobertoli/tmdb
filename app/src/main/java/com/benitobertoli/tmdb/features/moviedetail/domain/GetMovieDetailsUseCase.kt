package com.benitobertoli.tmdb.features.moviedetail.domain

import com.benitobertoli.tmdb.UseCase
import com.benitobertoli.tmdb.data.api.RestApi
import com.benitobertoli.tmdb.data.api.dto.MovieDetailsDto
import com.benitobertoli.tmdb.data.api.dto.NowShowingDto
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

class GetMovieDetailsUseCase
@Inject constructor(
        private val restApi: RestApi,
        @Named("subscribe") subscribeScheduler: Scheduler,
        @Named("observe") observeScheduler: Scheduler)
    : UseCase<MovieDetailsDto, Long>(subscribeScheduler, observeScheduler) {

    override fun buildUseCaseObservable(params: Long): Observable<MovieDetailsDto> {
        return restApi.getMovieDetails(params)
    }
}
