package com.benitobertoli.tmdb.features.movies

import android.arch.lifecycle.MutableLiveData
import com.benitobertoli.tmdb.DefaultObserver
import com.benitobertoli.tmdb.data.SingleLiveEvent
import com.benitobertoli.tmdb.data.api.dto.NowShowingDto
import com.benitobertoli.tmdb.exception.AppException
import com.benitobertoli.tmdb.exception.AppExceptionFactory
import com.benitobertoli.tmdb.features.movies.domain.GetNowShowingUseCase
import com.benitobertoli.tmdb.ui.BaseViewModel
import com.benitobertoli.tmdb.util.backdropSmall
import com.benitobertoli.tmdb.util.posterSmall
import com.benitobertoli.tmdb.util.withNotNullNorEmpty
import com.benitobertoli.tmdb.util.withNullOrEmpty
import java.util.*
import javax.inject.Inject

class MoviesViewModel
@Inject constructor(
        private val getNowShowing: GetNowShowingUseCase,
        private val appExceptionFactory: AppExceptionFactory
) : BaseViewModel() {

    val nowShowingData = MutableLiveData<List<MovieModel>>()
    val filterData = MutableLiveData<Pair<Calendar?, Calendar?>>()

    private var loading = false
    private var hasMorePages = true
    private var page = 1

    fun refreshIfEmpty(){
        nowShowingData.value.withNullOrEmpty { refresh() }
    }

    private fun refresh() {
        hasMorePages = true
        page = 1
        loadNextPage()
    }

    fun loadNextPage() {
        if (!hasMorePages || loading) return
        showLoading()
        loading = true
        getNowShowing.execute(page, buildMoviesObserver())
    }

    private fun showLoading() {
        if (page == 1) {
            showInlineLoading()
        }
    }

    private fun buildMoviesObserver(): DefaultObserver<NowShowingDto> {
        return object : DefaultObserver<NowShowingDto>(appExceptionFactory) {

            override fun onNext(nowShowing: NowShowingDto) {
                loading = false
                val movies = nowShowingData.value?.toMutableList() ?: mutableListOf()

                if (page == 1) {
                    // this is a refresh
                    // clear data
                    movies.clear()

                    showContent()
                }

                movies.addAll(nowShowing.movies.map {
                    MovieModel(
                            id = it.id,
                            title = it.title,
                            backdrop = it.backdrop?.backdropSmall(),
                            poster = it.poster?.posterSmall(),
                            releaseDate = it.releaseDate,
                            voteAverage = it.voteAverage
                    )
                })

                nowShowingData.value = movies

                if (nowShowing.totalPages > page) {
                    // more pages to load
                    page++
                } else {
                    hasMorePages = false
                }
            }

            override fun onError(e: AppException) {
                loading = false
                if (page == 1) {
                    showInlineMessage(e.userMessage) { loadNextPage() }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        getNowShowing.dispose()
    }
}
