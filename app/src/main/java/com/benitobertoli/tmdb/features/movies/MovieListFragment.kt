package com.benitobertoli.tmdb.features.movies

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import androidx.navigation.fragment.findNavController
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.DatePicker
import com.applandeo.materialcalendarview.builders.DatePickerBuilder
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener
import com.benitobertoli.tmdb.BaseFragment
import com.benitobertoli.tmdb.R
import com.benitobertoli.tmdb.ui.getViewModel
import com.benitobertoli.tmdb.ui.observe
import com.benitobertoli.tmdb.ui.observeNotNull
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_movie_list.*
import java.text.SimpleDateFormat
import java.util.*


class MovieListFragment : BaseFragment<MoviesViewModel>() {

    private var filterMenu: MenuItem? = null
    private var datePicker: DatePicker? = null
    private val releaseDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH)

    private val adapter = MoviesAdapter { movie ->
        findNavController().run {
            if (currentDestination?.id == R.id.movieListFragment) {
                // this check is done to suppress very quick taps where we will get two navigations
                // in which case the second will throw an exception
                val directions = MovieListFragmentDirections.actionMovieListFragmentToMovieDetailsFragment().setMovieId(movie.id)
                navigate(directions)
            }
        }
    }

    override fun inject() {
        AndroidSupportInjection.inject(this)
    }

    override fun getViewModel(): MoviesViewModel? {
        return activity?.getViewModel(viewModelFactory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return wrap(inflater.context, R.layout.fragment_movie_list, 0, true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        datePicker = buildDatePicker()
        setHasOptionsMenu(true)
        showContent()
        setupRecyclerView()
        bindMovieData()
        bindFilterData()
        clearFilter.setOnClickListener {
            getViewModel()?.filterData?.value = Pair(null, null)
            datePicker = buildDatePicker()
        }
        getViewModel()?.refreshIfEmpty()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_movie_list, menu)
        filterMenu = menu?.findItem(R.id.action_filter)
        updateFilterIcon()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_filter -> {
                datePicker?.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun updateFilterIcon() {
        if (getViewModel()?.filterData?.value?.first != null) {
            filterMenu?.setIcon(R.mipmap.ic_calendar_filter_on)
        } else {
            filterMenu?.setIcon(R.mipmap.ic_calendar_filter_off)
        }
    }

    private fun setupRecyclerView() {
        // layout manager
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager

        // dividers
        val horizontalDecoration = DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL)
        val horizontalDivider = ContextCompat.getDrawable(requireActivity(), R.drawable.divider_horizontal)
        horizontalDecoration.setDrawable(horizontalDivider!!)
        recyclerView.addItemDecoration(horizontalDecoration)

        // adapter
        recyclerView.adapter = adapter

        // infinite loading
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val visibleThreshold = 8
            var previousTotal = 0
            var lastItemPosition = 0
            var totalItemCount = 0

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                lastItemPosition = layoutManager.findLastVisibleItemPosition()
                totalItemCount = layoutManager.itemCount
                totalItemCount.takeIf { it > 0 }?.let {
                    if (totalItemCount > previousTotal) {
                        previousTotal = totalItemCount
                    }
                    if (lastItemPosition == totalItemCount - visibleThreshold - 1
                    || !recyclerView.canScrollVertically(1)) {
                        // end has been reached
                        // load new data
                        getViewModel()?.loadNextPage()
                    }
                }
            }
        })
    }

    private fun bindMovieData() {
        observeNotNull(getViewModel()?.nowShowingData) {
            adapter.setMovies(it)
        }
    }

    private fun bindFilterData() {
        observe(getViewModel()?.filterData) { dates ->
            if (dates == null) {
                filterLayout.visibility = View.GONE
                adapter.clearFilter()
            } else {
                adapter.applyFilter(dates.first, dates.second)
                applyFilterMessage(dates.first, dates.second)
            }

            updateFilterIcon()
        }
    }

    private fun applyFilterMessage(start: Calendar?, end: Calendar?){
        when {
            start == null -> filterLayout.visibility = View.GONE
            end == null -> {
                filterMessage.text = String.format(getString(R.string.filter_message_single), releaseDateFormat.format(start.time))
                filterLayout.visibility = View.VISIBLE
            }
            else -> {
                filterMessage.text = String.format(getString(R.string.filter_message_range),
                        releaseDateFormat.format(start.time), releaseDateFormat.format(end.time))
                filterLayout.visibility = View.VISIBLE
            }
        }

    }

    private fun buildDatePicker(): DatePicker {
        return DatePickerBuilder(requireActivity(), onSelectedDate)
                .headerColor(R.color.grey_dark)
                .headerLabelColor(R.color.grey_400)
                .abbreviationsBarColor(R.color.grey_medium)
                .abbreviationsLabelsColor(R.color.grey_400)
                .pagesColor(R.color.grey_primary)
                .selectionColor(R.color.yellow_accent)
                .todayLabelColor(R.color.yellow_accent)
                .selectionLabelColor(R.color.white)
                .daysLabelsColor(R.color.grey_400)
                .anotherMonthsDaysLabelsColor(R.color.grey_700)
                .pickerType(CalendarView.RANGE_PICKER)
                .dialogButtonsColor(R.color.grey_400)
                .build()
    }

    private val onSelectedDate = OnSelectDateListener { calendars ->
        var start: Calendar? = null
        var end: Calendar? = null

        if (calendars.isNotEmpty()) {
            start = calendars.first()
        }

        if (calendars.size > 1) {
            end = calendars.last()
        }

        getViewModel()?.filterData?.value = Pair(start, end)
    }

}
