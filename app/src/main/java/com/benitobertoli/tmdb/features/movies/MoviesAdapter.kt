package com.benitobertoli.tmdb.features.movies

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.benitobertoli.tmdb.BR
import com.benitobertoli.tmdb.R
import com.benitobertoli.tmdb.util.sameDay
import java.text.SimpleDateFormat
import java.util.*

class MoviesAdapter(
        private var original: List<MovieModel> = emptyList(),
        private val callback: ((MovieModel) -> Unit)? = null
) : RecyclerView.Adapter<MoviesAdapter.MovieVH>() {

    private val releaseDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH)
    private var filtered: MutableList<MovieModel> = mutableListOf()

    var startCalendar: Calendar? = null
    var endCalendar: Calendar? = null

    init {
        filtered.addAll(original)
    }

    fun clearFilter() {
        applyFilter(null, null)
    }

    fun applyFilter(start: Calendar?, end: Calendar? = null) {
        startCalendar = start
        endCalendar = end
        showFiltered(getFilteredList())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieVH {
        val viewDataBinding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.row_movie, parent, false)
        return MovieVH(viewDataBinding)
    }

    override fun getItemCount(): Int = filtered.size

    override fun onBindViewHolder(holder: MovieVH, position: Int) {
        val movie = filtered[position]
        holder.bind(movie, releaseDateFormat)
        holder.itemView.setOnClickListener { callback?.invoke(movie) }
    }

    class MovieVH(private val viewDataBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {
        fun bind(movie: MovieModel, dateFormat: SimpleDateFormat) {
            viewDataBinding.setVariable(BR.movie, movie)
            viewDataBinding.setVariable(BR.dateFormat, dateFormat)
            viewDataBinding.executePendingBindings()
        }
    }

    fun setMovies(newMovies: List<MovieModel>) {
        original = newMovies.toMutableList()
        showFiltered(getFilteredList())
    }

    private fun getFilteredList(): List<MovieModel> {
        val calendar = Calendar.getInstance()
        val start = startCalendar
        val end = endCalendar

        if (start != null && end != null) {
            return original.filter { movie ->
                movie.releaseDate >= start.time && movie.releaseDate <= end.time
            }
        }

        if (start != null && end == null) {
            // single date
            return original.filter { movie ->
                calendar.time = movie.releaseDate
                calendar.sameDay(start)
            }
        }

        return original
    }

    private fun showFiltered(newFiltered: List<MovieModel>) {
        val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun getOldListSize(): Int = filtered.size

            override fun getNewListSize(): Int = newFiltered.size

            override fun areItemsTheSame(oldItemPos: Int, newItemPosition: Int): Boolean {
                return filtered[oldItemPos].id == newFiltered[newItemPosition].id
            }

            override fun areContentsTheSame(oldItemPos: Int, newItemPosition: Int): Boolean {
                return filtered[oldItemPos].id == newFiltered[newItemPosition].id
                        && filtered[oldItemPos].title == newFiltered[newItemPosition].title
                        && filtered[oldItemPos].voteAverage == newFiltered[newItemPosition].voteAverage
                        && filtered[oldItemPos].releaseDate == newFiltered[newItemPosition].releaseDate
                        && filtered[oldItemPos].poster == newFiltered[newItemPosition].poster
            }

        })

        filtered = newFiltered.toMutableList()
        result.dispatchUpdatesTo(this)
    }


}