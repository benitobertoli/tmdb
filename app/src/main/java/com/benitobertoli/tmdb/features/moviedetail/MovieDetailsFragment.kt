package com.benitobertoli.tmdb.features.moviedetail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.benitobertoli.tmdb.BaseFragment
import com.benitobertoli.tmdb.R
import com.benitobertoli.tmdb.data.api.dto.CompanyDto
import com.benitobertoli.tmdb.data.api.dto.GenreDto
import com.benitobertoli.tmdb.ui.getViewModel
import com.benitobertoli.tmdb.ui.observeNotNull
import com.benitobertoli.tmdb.util.backdropLarge
import com.benitobertoli.tmdb.util.format
import com.benitobertoli.tmdb.util.logoSmall
import com.benitobertoli.tmdb.util.posterSmall
import com.facebook.drawee.view.SimpleDraweeView
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_movie_details.*
import java.text.SimpleDateFormat
import java.util.*


class MovieDetailsFragment : BaseFragment<MovieDetailsViewModel>() {

    private val releaseDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH)

    override fun inject() {
        AndroidSupportInjection.inject(this)
    }

    override fun getViewModel(): MovieDetailsViewModel? {
        return activity?.getViewModel(viewModelFactory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return wrap(inflater.context, R.layout.fragment_movie_details, 0, true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        actionbar?.title = ""
        showContent()
        bindMovieData()
        val movieId = MovieDetailsFragmentArgs.fromBundle(arguments).movieId
        getViewModel()?.getMovieDetails(movieId)
    }

    private fun bindMovieData() {
        observeNotNull(getViewModel()?.movieData) { movie ->
            actionbar?.title = movie.title

            poster.setImageURI(movie.poster?.posterSmall())
            movie.backdrop?.backdropLarge()?.let {
                backdrop.setImageURI(it)
                backdrop.visibility = View.VISIBLE
            }

            movie.homepage?.let { url ->
                homepage.setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url))) }
                homepage.visibility = View.VISIBLE
            }

            overview.text = movie.overview

            setRating(movie.voteAverage)
            voteCount.text = movie.voteCount.format()


            summary.text = movie.overview
            movie.tagline?.takeIf { it.isNotBlank() }?.let {
                tagline.text = it
                tagline.visibility = View.VISIBLE
                taglineLabel.visibility = View.VISIBLE
            }

            displayGenres(movie.genres)

            releaseDate.text = releaseDateFormat.format(movie.releaseDate)
            spokenLanguage.text = movie.spokenLanguages.joinToString()

            displayCompanies(movie.productionCompanies)
        }
    }

    private fun setRating(value: Double) {
        val vote = "$value/10"
        val spannable = SpannableString(vote)
        spannable.setSpan(RelativeSizeSpan(0.6f), vote.length - 3, vote.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        rating.text = spannable
    }

    private fun displayGenres(genres: List<GenreDto>) {
        genreLayout.removeAllViews()
        genres.forEach { genre ->
            val button = layoutInflater.inflate(R.layout.button_genre, genreLayout, false) as Button
            button.text = genre.name
            genreLayout.addView(button)
        }
    }

    private fun displayCompanies(companies: List<CompanyDto>) {
        companiesLayout.removeAllViews()
        companies.forEach { company ->

            val layout = layoutInflater.inflate(R.layout.imageview_company, genreLayout, false)
            val imageView = layout.findViewById<SimpleDraweeView>(R.id.image)
            val textView = layout.findViewById<TextView>(R.id.name)
            imageView.setImageURI(company.logoPath?.logoSmall())
            if(company.logoPath == null){
                textView.text = company.name
            }
            companiesLayout.addView(layout)
        }
    }
}
