package com.benitobertoli.tmdb.features.movies

import java.util.*

data class MovieModel(
        val id: Long,
        val title: String,
        val backdrop: String?,
        val poster: String?,
        val releaseDate: Date,
        val voteAverage: Double
)