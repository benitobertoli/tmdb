package com.benitobertoli.tmdb.features.movies.domain

import com.benitobertoli.tmdb.UseCase
import com.benitobertoli.tmdb.data.api.RestApi
import com.benitobertoli.tmdb.data.api.dto.NowShowingDto
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

class GetNowShowingUseCase
@Inject constructor(
        private val restApi: RestApi,
        @Named("subscribe") subscribeScheduler: Scheduler,
        @Named("observe") observeScheduler: Scheduler)
    : UseCase<NowShowingDto, Int>(subscribeScheduler, observeScheduler) {

    override fun buildUseCaseObservable(params: Int): Observable<NowShowingDto> {
        return restApi.getNowShowing(params)
    }
}
