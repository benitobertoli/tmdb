package com.benitobertoli.tmdb.features.movies

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.benitobertoli.tmdb.BaseActivity
import com.benitobertoli.tmdb.R
import dagger.android.AndroidInjection

class MoviesActivity : BaseActivity() {

    override fun inject() {
        AndroidInjection.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list, false, true, R.layout.toolbar_default)
        NavigationUI.setupActionBarWithNavController(this, findNavController(R.id.nav_host_main))
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_main).navigateUp()
    }
}
