package com.benitobertoli.tmdb.exception

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.benitobertoli.tmdb.BuildConfig
import com.benitobertoli.tmdb.R
import com.benitobertoli.tmdb.data.api.dto.ErrorDto
import com.benitobertoli.tmdb.exception.AppException.Code.DATA
import com.benitobertoli.tmdb.exception.AppException.Code.NETWORK
import com.benitobertoli.tmdb.exception.AppException.Code.UNEXPECTED
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class AppExceptionFactory
@Inject constructor(context: Context,
                    @field:[Named("Api")] private val gson: Gson) {

    private val networkErrorMessage: String = context.getString(R.string.error_network)
    private val unexpectedErrorMessage: String = context.getString(R.string.error_unexpected)

    fun make(t: Throwable): AppException {
        if (t is AppException) return t
        val exception = when (t) {
            is HttpException -> try {
                val apiResponse = gson.fromJson(t.response().errorBody()?.string(), ErrorDto::class.java)
                AppException(apiResponse?.code ?: UNEXPECTED,
                        apiResponse?.message ?: unexpectedErrorMessage, apiResponse?.errors?.joinToString(), t)
            } catch (e: IOException) {
                AppException(DATA, unexpectedErrorMessage, t.message, e)
            } catch (e: JsonSyntaxException) {
                AppException(DATA, unexpectedErrorMessage, t.message, e)
            }
            is IOException -> AppException(NETWORK, networkErrorMessage, t.message, t)
            else -> AppException(UNEXPECTED, unexpectedErrorMessage, t.message, t)
        }

        if (BuildConfig.DEBUG) exception.printStackTrace()

        return exception
    }

}