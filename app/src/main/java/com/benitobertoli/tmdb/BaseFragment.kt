package com.benitobertoli.tmdb

import android.app.ProgressDialog
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.benitobertoli.tmdb.ui.BaseViewModel
import com.benitobertoli.tmdb.ui.blocks.LoadingLayout
import com.benitobertoli.tmdb.ui.blocks.LoadingView
import com.benitobertoli.tmdb.ui.blocks.ToolbarLayout
import com.benitobertoli.tmdb.ui.observe
import com.benitobertoli.tmdb.ui.observeNotNull
import com.benitobertoli.tmdb.util.DialogUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseFragment<V : BaseViewModel> : Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    protected val actionbar: ActionBar? by lazy { getActionBar() }

    private var toolbarLayout: ToolbarLayout? = null
    protected var toolbar: Toolbar? = null
        get() = toolbarLayout?.toolbar
        private set
    private var loadingLayout: LoadingLayout? = null
    protected var loadingView: LoadingView? = null
        get() = loadingLayout?.loadingView

    protected var progressDialog: ProgressDialog? = null

    private var rxDisposables: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    /**
     * Dagger 2 relies on strongly typed classes so we must explicitly inject
     * each subclass.
     */
    protected abstract fun inject()

    /**
     * Toolbar might be shared between multiple fragments. Configure it here.
     */
    open fun configureToolbar() {}

    /**
     * Override this method to set the ViewModel
     */
    open fun getViewModel(): V? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureToolbar()
        bindBaseViewModel()
    }

    fun wrap(context: Context, view: View): View {
        loadingLayout = LoadingLayout(context)
        loadingLayout?.let {
            it.addView(view, 0)
            return it
        }
        throw IllegalStateException("LoadingLayout was not created")
    }

    fun wrap(context: Context, view: View, wrapLoading: Boolean, wrapToolbar: Boolean, toolbarLayoutId: Int, toolbarId: Int): View {
        var v = view
        if (wrapLoading) {
            loadingLayout = LoadingLayout(context)
            loadingLayout?.let {
                it.addView(v, 0)
                v = it
            }
        }

        if (wrapToolbar) {
            toolbarLayout = ToolbarLayout(context, toolbarLayoutId, toolbarId)
            toolbarLayout?.let {
                it.addView(v, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                v = it
            }
        }

        return v
    }

    fun wrap(context: Context, view: View, toolbarLayoutId: Int = 0, wrapLoading: Boolean = false): View {
        return wrap(context, view, wrapLoading, toolbarLayoutId != 0, toolbarLayoutId, R.id.toolbar)
    }

    fun wrap(context: Context, layoutResId: Int, toolbarLayoutId: Int = 0, wrapLoading: Boolean = false): View {
        val view = LayoutInflater.from(context).inflate(layoutResId, LinearLayout(context), false)
        return wrap(context, view, toolbarLayoutId, wrapLoading)
    }

    protected fun addDisposable(disposable: Disposable) {
        if (rxDisposables == null) {
            rxDisposables = CompositeDisposable()
        }
        rxDisposables?.add(disposable)
    }

    override fun onDetach() {
        super.onDetach()
        rxDisposables?.dispose()
    }

    private fun bindBaseViewModel() {

        observe(getViewModel()?.contentVisible) {
            if (it == true) showContent() else showInlineLoading()
        }

        observeNotNull(getViewModel()?.messageData) {
            val messageRes = getStringIfNotNull(it.messageRes)
            val message = messageRes ?: it.message ?: ""
            val actionNameRes = getStringIfNotNull(it.actionNameRes)
            val actionName = actionNameRes ?: it.actionName ?: ""
            if (it.inline) {
                if (it.action == null) {
                    showInlineMessage(message)
                } else {
                    showInlineMessageWithAction(message, actionName, it.action)
                }
            } else {
                if (it.action == null) {
                    showMessage(message)
                } else {
                    showMessageWithAction(message, actionName, it.action)
                }
            }
        }

        observeNotNull(getViewModel()?.progressData) {
            val messageRes = getStringIfNotNull(it.messageRes)
            val message = messageRes ?: it.message ?: ""
            if (it.show) {
                showProgressDialog(message)
            } else {
                hideProgressDialog()
            }
        }

    }

    private fun getStringIfNotNull(resId: Int?): String? {
        return if (resId != null) getString(resId) else null
    }


    protected fun showContent() {
        loadingLayout?.showContent()
    }

    protected fun showInlineLoading() {
        loadingLayout?.showLoadingView()
        loadingView?.loading(true)
    }

    protected fun showInlineMessage(message: String) {
        loadingLayout?.showLoadingView()
        loadingView?.loading(false)
                ?.message(message)
                ?.displayButton(false)
    }

    protected fun showInlineMessageWithAction(message: String, actionName: String, action: (() -> Unit)?) {
        loadingLayout?.showLoadingView()
        loadingView?.loading(false)
                ?.message(message)
                ?.displayButton(true)
                ?.buttonText(actionName)
                ?.buttonClickListener(View.OnClickListener { action?.invoke() })
    }

    protected fun showProgressDialog(message: String) {
        activity?.let { progressDialog = DialogUtils.showLoadingDialog(it, message) }
    }

    protected fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

    protected fun showMessage(message: String) {
        view?.let { view ->
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
        }
    }

    protected fun showMessageWithAction(message: String, actionName: String, action: (() -> Unit)?) {
        view?.let { view ->
            Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(actionName) { action?.invoke() }
                    .show()
        }
    }

    private fun getActionBar(): ActionBar? {
        return activity?.let {
            (it as? AppCompatActivity)?.supportActionBar
        }
    }
}
