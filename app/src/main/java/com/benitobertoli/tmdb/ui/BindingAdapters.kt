package com.benitobertoli.tmdb.ui

import android.databinding.BindingAdapter
import android.view.View
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter("imageUrl")
fun loadImage(simpleDraweeView: SimpleDraweeView, url: String?) {
    simpleDraweeView.setImageURI(url)
}

@BindingAdapter("goneIfNull")
fun goneIfNull(view: View, obj: Any?) {
    view.visibility = if (obj == null) View.GONE else View.VISIBLE
}

@BindingAdapter("rating")
fun rating(textView: TextView, rating: Double) {
    textView.text = rating.toString()
}

@BindingAdapter(value = ["date", "dateFormat"])
fun dateText(textView: TextView, date: Date, dateFormat: SimpleDateFormat) {
    textView.text = dateFormat.format(date)
}