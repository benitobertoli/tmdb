package com.benitobertoli.tmdb.ui

import android.arch.lifecycle.*
import android.support.v4.app.FragmentActivity

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory, key: String? = null): T {
    return if (key == null) {
        ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    } else {
        ViewModelProviders.of(this, viewModelFactory)[key, T::class.java]
    }
}

inline fun <reified T : ViewModel> FragmentActivity.withViewModel(viewModelFactory: ViewModelProvider.Factory, body: T.() -> Unit): T {
    val vm = getViewModel<T>(viewModelFactory)
    vm.body()
    return vm
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L?, body: (T?) -> Unit) {
    liveData?.observe(this, Observer(body))
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observeNotNull(liveData: L?, body: (T) -> Unit) {
    liveData?.observe(this, Observer { it?.let(body) })
}