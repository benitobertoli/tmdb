package com.benitobertoli.tmdb.ui

import android.support.annotation.StringRes

data class Message(
        val inline: Boolean,
        @StringRes val messageRes: Int?,
        val message: String?,
        @StringRes val actionNameRes: Int?,
        val actionName: String?,
        val action: (() -> Unit)? = null
)