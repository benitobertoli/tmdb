package com.benitobertoli.tmdb.ui

import android.support.annotation.StringRes

data class ProgressString(
        val show: Boolean = true,
        @StringRes val message: String
)