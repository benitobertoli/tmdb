package com.benitobertoli.tmdb.ui

import android.arch.lifecycle.ViewModel
import android.support.annotation.StringRes
import com.benitobertoli.tmdb.R
import com.benitobertoli.tmdb.data.SingleLiveEvent
import javax.inject.Inject


open class BaseViewModel
@Inject constructor()
    : ViewModel() {
    val contentVisible = SingleLiveEvent<Boolean>()
    val messageData = SingleLiveEvent<Message>()
    val progressData = SingleLiveEvent<Progress>()

    fun showContent() {
        contentVisible.value = true
    }

    fun showInlineLoading() {
        contentVisible.value = false
    }

    fun showInlineMessage(message: String, actionName: String, action: (() -> Unit)? = null) {
        messageData.value = Message(true, null, message, null, actionName, action)
    }

    fun showInlineMessage(@StringRes messageRes: Int, actionName: String, action: (() -> Unit)? = null) {
        messageData.value = Message(true, messageRes, null, null, actionName, action)
    }

    fun showInlineMessage(message: String, @StringRes actionNameRes: Int = R.string.retry, action: (() -> Unit)? = null) {
        messageData.value = Message(true, null, message, actionNameRes, null, action)
    }

    fun showInlineMessage(@StringRes messageRes: Int, @StringRes actionNameRes: Int = R.string.retry, action: (() -> Unit)? = null) {
        messageData.value = Message(true, messageRes, null, actionNameRes, null, action)
    }

    fun showMessage(message: String, actionName: String, action: (() -> Unit)? = null) {
        messageData.value = Message(false, null, message, null, actionName, action)
    }

    fun showMessage(message: String, @StringRes actionNameRes: Int = R.string.retry, action: (() -> Unit)? = null) {
        messageData.value = Message(false, null, message, actionNameRes, null, action)
    }

    fun showProgressDialog(message: String) {
        progressData.value = Progress(true, null, message)
    }

    fun showProgressDialog(@StringRes messageRes: Int = R.string.loading_) {
        progressData.value = Progress(true, messageRes, null)
    }

    fun hideProgressDialog() {
        progressData.value = Progress(false, null, null)
    }

}