package com.benitobertoli.tmdb.ui

import android.support.annotation.StringRes

data class Progress(
        val show: Boolean = true,
        @StringRes val messageRes: Int?,
        val message: String?
)