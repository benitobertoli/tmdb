package com.benitobertoli.tmdb

import android.app.Application
import android.content.Context
import com.benitobertoli.tmdb.data.DataModule
import com.benitobertoli.tmdb.ui.ViewModelModule
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [DataModule::class, ViewModelModule::class])
class AppModule(application: App) {
    private val application: Application

    init {
        this.application = application
    }

    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return application
    }


    @Provides
    @Named("subscribe")
    @Singleton
    internal fun provideSubscribeScheduler(): Scheduler {
        return Schedulers.newThread()
    }

    @Provides
    @Named("observe")
    @Singleton
    internal fun provideObserveScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

}
