package com.benitobertoli.tmdb

import com.benitobertoli.tmdb.features.moviedetail.MovieDetailsFragment
import com.benitobertoli.tmdb.features.movies.MovieListFragment
import com.benitobertoli.tmdb.features.movies.MoviesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class InjectorsModule {

    @ContributesAndroidInjector
    internal abstract fun moviesActivity(): MoviesActivity

    @ContributesAndroidInjector
    internal abstract fun movieListFragment(): MovieListFragment

    @ContributesAndroidInjector
    internal abstract fun movieDetailsFragment(): MovieDetailsFragment

}