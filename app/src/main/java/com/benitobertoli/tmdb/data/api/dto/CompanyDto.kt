package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CompanyDto(
        @field:[Expose SerializedName("id")]
        val id: Long,
        @field:[Expose SerializedName("name")]
        val name: String,
        @field:[Expose SerializedName("logo_path")]
        val logoPath: String?,
        @field:[Expose SerializedName("origin_country")]
        val originCountry: String
) {
    override fun toString(): String {
        return name
    }
}