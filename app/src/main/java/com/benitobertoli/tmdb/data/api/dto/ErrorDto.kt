package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorDto(
        @field:[Expose SerializedName("status_code")]
        var code: Int?,
        @field:[Expose SerializedName("status_message")]
        var message: String? = null,
        @field:[Expose SerializedName("errors")]
        var errors: List<String>? = null
)