package com.benitobertoli.tmdb.data.api

object ApiContract {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/"
    const val API_KEY = "b0c6d6f96597f53eea18ef0166c96906"

    object BACKDROP {
        const val W_300 = "w300"
        const val W_780 = "w780"
        const val W_1280 = "w1280"
        const val ORIGINAL = "original"
    }

    object POSTER {
        const val W_92 = "w92"
        const val W_154 = "w154"
        const val W_185 = "w185"
        const val W_342 = "w342"
        const val W_500 = "w500"
        const val W_780 = "w780"
        const val ORIGINAL = "original"
    }

    object LOGO {
        const val W_45 = "w45"
        const val W_92 = "w92"
        const val W_154 = "w154"
        const val W_185 = "w185"
        const val W_300 = "w300"
        const val W_500 = "w500"
        const val ORIGINAL = "original"
    }
}