package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NowShowingDto(
        @field:[Expose SerializedName("results")]
        val movies: List<MovieDto>,
        @field:[Expose SerializedName("page")]
        val page: Int,
        @field:[Expose SerializedName("total_results")]
        val totalResults: Int,
        @field:[Expose SerializedName("total_pages")]
        val totalPages: Int
)