package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LanguageDto(
        @field:[Expose SerializedName("iso_639_1")]
        val id: String,
        @field:[Expose SerializedName("name")]
        val name: String
) {
    override fun toString(): String {
        return name
    }
}