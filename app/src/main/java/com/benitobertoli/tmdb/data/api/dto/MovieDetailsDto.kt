package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieDetailsDto(
        @field:[Expose SerializedName("vote_count")]
        val voteCount: Int,
        @field:[Expose SerializedName("id")]
        val id: Long,
        @field:[Expose SerializedName("video")]
        val video: Boolean,
        @field:[Expose SerializedName("vote_average")]
        val voteAverage: Double,
        @field:[Expose SerializedName("title")]
        val title: String,
        @field:[Expose SerializedName("popularity")]
        val popularity: Double,
        @field:[Expose SerializedName("poster_path")]
        val poster: String?,
        @field:[Expose SerializedName("original_language")]
        val originalLanguage: String,
        @field:[Expose SerializedName("original_title")]
        val originalTitle: String,
        @field:[Expose SerializedName("backdrop_path")]
        val backdrop: String?,
        @field:[Expose SerializedName("adult")]
        val adult: Boolean,
        @field:[Expose SerializedName("overview")]
        val overview: String?,
        @field:[Expose SerializedName("release_date")]
        val releaseDate: Date,
        @field:[Expose SerializedName("budget")]
        val budget: Long,
        @field:[Expose SerializedName("revenue")]
        val revenue: Long,
        @field:[Expose SerializedName("genres")]
        val genres: List<GenreDto>,
        @field:[Expose SerializedName("homepage")]
        val homepage: String?,
        @field:[Expose SerializedName("production_companies")]
        val productionCompanies: List<CompanyDto>,
        @field:[Expose SerializedName("production_countries")]
        val productionCountries: List<CountryDto>,
        @field:[Expose SerializedName("spoken_languages")]
        val spokenLanguages: List<LanguageDto>,
        @field:[Expose SerializedName("tagline")]
        val tagline: String?
)