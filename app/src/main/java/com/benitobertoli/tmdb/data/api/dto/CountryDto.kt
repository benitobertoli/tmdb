package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CountryDto(
        @field:[Expose SerializedName("iso_3166_1")]
        val id: String,
        @field:[Expose SerializedName("name")]
        val name: String
) {
    override fun toString(): String {
        return name
    }
}