package com.benitobertoli.tmdb.data.api.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieDto(
        @field:[Expose SerializedName("vote_count")]
        val voteCount: Int,
        @field:[Expose SerializedName("id")]
        val id: Long,
        @field:[Expose SerializedName("video")]
        val video: Boolean,
        @field:[Expose SerializedName("vote_average")]
        val voteAverage: Double,
        @field:[Expose SerializedName("title")]
        val title: String,
        @field:[Expose SerializedName("popularity")]
        val popularity: Double,
        @field:[Expose SerializedName("poster_path")]
        val poster: String?,
        @field:[Expose SerializedName("original_language")]
        val originalLanguage: String,
        @field:[Expose SerializedName("original_title")]
        val originalTitle: String,
        @field:[Expose SerializedName("backdrop_path")]
        val backdrop: String?,
        @field:[Expose SerializedName("adult")]
        val adult: Boolean,
        @field:[Expose SerializedName("overview")]
        val overview: String?,
        @field:[Expose SerializedName("release_date")]
        val releaseDate: Date
)