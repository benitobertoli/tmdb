package com.benitobertoli.tmdb.data.api

import com.benitobertoli.tmdb.data.api.dto.MovieDetailsDto
import com.benitobertoli.tmdb.data.api.dto.NowShowingDto
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RestApi {

    @GET("movie/now_playing")
    fun getNowShowing(
            @Query("page") page: Int): Observable<NowShowingDto>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
            @Path("movie_id") movieId: Long): Observable<MovieDetailsDto>
}